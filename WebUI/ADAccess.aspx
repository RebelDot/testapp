﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ADAccess.aspx.cs" Inherits="AppPortal.WebUI.ADAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <br/>
        <br/>
        <asp:ListBox ID="ListBox1" runat="server"></asp:ListBox>
        <br/>
        <br/>
        <br/>
        All AD groups:
        <br/>
        <br/>
        <asp:ListBox ID="ListBox2" runat="server"></asp:ListBox>
        <br/>
        <br/>
        <br/>
        All local groups:
        <br/>
        <br/>
        <asp:ListBox ID="ListBox3" runat="server"></asp:ListBox>
        <br/>
        <br/>
        <br/>
        All AD users:
        <br/>
        <br/>
        <asp:ListBox ID="ListBox4" runat="server"></asp:ListBox>
        <br/>
        <br/>
        <br/>
        All local users:
        <br/>
        <br/>
        <asp:ListBox ID="ListBox5" runat="server"></asp:ListBox>
        <br/>
        <br/>
        <br/>
        <%--All users from Developers group:
        <br/>
        <br/>
        <asp:ListBox ID="ListBox6" runat="server"></asp:ListBox>--%>
    </div>
    </form>
</body>
</html>
