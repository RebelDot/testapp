﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalGroupMembers.aspx.cs"
    Inherits="AppPortal.WebUI.LocalGroups.LocalGroupMembers" MasterPageFile="~/WebUI/Main.Master"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphTitle">
    View and edit Local Group Members</asp:Content>
<asp:Content ID="cntBreadCrumb" ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblBreadCrumb" runat="server" Text="Edit Local Group Members" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="server">

    <script type="text/javascript" language="javascript">
        function confApprove()
        {
            resp = window.confirm ('Are you sure you want to remove this Site from the bathroom?');
            return resp;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphBody">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="text-align: center">
                <asp:HiddenField ID="hdnLocalGroupID" runat="server" />
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlLocalGroups" runat="server" CssClass="panelGW">
                                <asp:Label ID="lblTitlePanel" runat="server" Text="Local Groups Members:"></asp:Label>
                            </asp:Panel>
                            <asp:GridView ID="grdMembers" runat="server" AutoGenerateColumns="False" CssClass="gw"
                                OnRowDeleting="grdMembers_RowDeleting" DataKeyNames="AccountID" OnRowCancelingEdit="grdMembers_RowCancelingEdit"
                                OnRowEditing="grdMembers_RowEditing" OnRowUpdating="grdMembers_RowUpdating">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlName" DataTextField="Name" 
                                                CausesValidation="true" ValidationGroup="VG">
                                            </asp:DropDownList>
                                            <asp:CustomValidator ID="vldUniqueName" runat="server" ControlToValidate="ddlName"
                                                ErrorMessage="This account is already a member of the local group"
                                                ValidationGroup="VG">*</asp:CustomValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Domain">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDomainController" runat="server" Text='<%#Eval("Domain") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label runat="server" ID="lblDomainController" Text='<%#Eval("Domain") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CheckBoxField DataField="IsGroup" ReadOnly="True" HeaderText="Is Group" />
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:ImageButton runat="server" CommandName="update" ID="btnUpdate" ImageUrl="~/WebUI/img/save.jpg" 
                                                CausesValidation="true" ValidationGroup="VG"/>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" CommandName="delete" ID="btnDelete" ImageUrl="~/WebUI/img/delete.jpg"
                                                OnClientClick="return confApprove()" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton runat="server" CommandName="cancel" ID="btnCancel" ImageUrl="~/WebUI/img/cancel.jpg" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="gwAltItem" />
                                <HeaderStyle CssClass="gwHeader" />
                                <RowStyle CssClass="gwItem" />
                                <SelectedRowStyle CssClass="gwSelItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div style="float: left">
                                <asp:ImageButton ID="btnBack" runat="server" ToolTip="Back" OnClick="btnBack_Click"
                                    ImageUrl="~/WebUI/img/backs.jpg" /></div>
                            <asp:ImageButton ID="btnAddMember" runat="server" ImageUrl="~/WebUI/img/Add.gif"
                                ToolTip="Add Members" OnClick="btnAddMember_Click" />&nbsp
                        </td>
                    </tr>
                </table>
                <asp:ValidationSummary ID="vsLocalGroupMembers" ValidationGroup="VG" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
