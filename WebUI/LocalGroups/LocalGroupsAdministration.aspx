﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalGroupsAdministration.aspx.cs"
    Inherits="AppPortal.WebUI.LocalGroups.LocalGroupsAdministration" MasterPageFile="~/WebUI/Main.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphTitle">
    Local Group Administration</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="server">

    <script type="text/javascript" language="javascript">
        function confApprove()
        {
            resp = window.confirm ('Are you sure you want to delete this group?');
            return resp;
        }

    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblBreadCrumb" runat="server" Text="Local Groups Administration" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphBody">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="pnlLocalGroups" runat="server" CssClass="panelGW">
                            <asp:Label ID="lblPanel" runat="server" Text="All configured Local Groups"></asp:Label>
                        </asp:Panel>
                        <asp:GridView ID="grdLocalGroups" runat="server" CssClass="gw" AutoGenerateColumns="False"
                            DataKeyNames="LocalGroupID" OnRowDeleting="grdLocalGroups_RowDeleting" OnRowDataBound="grdLocalGroups_RowDataBound"
                            OnRowCancelingEdit="grdLocalGroups_RowCancelingEdit" OnRowEditing="grdLocalGroups_RowEditing"
                            OnRowUpdating="grdLocalGroups_RowUpdating">
                            <Columns>
                                <asp:TemplateField HeaderText="Grup Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("GroupName") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtName" Text='<%#Bind("GroupName") %>' 
                                            MaxLength="50" CausesValidation="true" ValidationGroup="VG" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="vldName" runat="server" ControlToValidate="txtName" 
                                            ErrorMessage="The Local Group Name is required" ValidationGroup="VG">*</asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="vldUniqueName" runat="server" ControlToValidate="txtName"
                                            ErrorMessage="The Local Group Name must be unique" ValidationGroup="VG">*</asp:CustomValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtDescription" Text='<%#Bind("Description") %>'
                                            MaxLength="200"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkMembers" runat="server" ImageUrl="~/WebUI/img/users.png">View and Edit Group Members</asp:HyperLink>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkSites" runat="server" ImageUrl="~/WebUI/img/site.jpg">View and Edit Sites Permissions for Group</asp:HyperLink>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="edit" ID="btnEdit" ImageUrl="~/WebUI/img/edit.jpg" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="update" ID="btnUpdate" CausesValidation="true" 
                                            ImageUrl="~/WebUI/img/save.jpg" ValidationGroup="VG" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="delete" ID="btnDelete" ImageUrl="~/WebUI/img/delete.jpg"
                                            OnClientClick="return confApprove()" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="cancel" ID="btnCancel" ImageUrl="~/WebUI/img/cancel.jpg" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="gwAltItem" />
                            <HeaderStyle CssClass="gwHeader" />
                            <RowStyle CssClass="gwItem" />
                            <SelectedRowStyle CssClass="gwSelItem" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:ImageButton ID="btnAddGroup" runat="server" ImageUrl="~/WebUI/img/Add.gif" ToolTip="Add Group"
                            OnClick="BtnAddGroup_Clicked" />
                    </td>
                </tr>
            </table>
            <asp:ValidationSummary ID="vsLocalGroupAdministration" runat="server" ValidationGroup="VG" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
