<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AppPortal.WebUI.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="~/WebUI/css/main.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:100%;text-align:center">
        <table>            
            <tr>
                <td class="panel">
                    Username:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtUser"></asp:TextBox>
                </td>
            </tr>
			<tr>
				<td class="panel">
                    Password:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPassword"></asp:TextBox>
                </td>
			</tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button runat="server" ID="btnLogin" Text="Login" OnClick="btnLogin_Click" Style="width: 100%" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
