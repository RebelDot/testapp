<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="AppPortal.WebUI.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width=100%; height=100%; vertical-align:middle; text-align:center; display:block; border:1px solid red">
        Sorry, but you don't have enough permissions to access this page.<br />
        Click <a href="/WebUI/Default.aspx">here</a> to access the default page.
    </div>
    </form>
</body>
</html>
