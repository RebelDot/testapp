﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SitesList.aspx.cs" Inherits="AppPortal.WebUI.Sites.SitesList"
    MasterPageFile="~/WebUI/Main.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphTitle">
    List of sites</asp:Content>
<asp:Content ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="cntBreadCrumb" ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblBreadCrumb" runat="server" Text="Edit Sites" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphBody">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>            
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="pnlADSites" runat="server" CssClass="panelGW">
                            <asp:Label ID="lblTitlePanel" runat="server" Text="All Sites"></asp:Label>
                        </asp:Panel>
                        <asp:GridView runat="server" ID="gwSites" AutoGenerateColumns="false" CssClass="gw"
                            OnRowEditing="gwSites_RowEditing" OnRowCancelingEdit="gwSites_RowCancelingEdit"
                            OnRowDeleting="gwSites_RowDeleting" OnRowUpdating="gwSites_RowUpdating" DataKeyNames="SiteID"
                            OnRowCommand="gwSites_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Site name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtName" Text='<%#Bind("Name") %>' MaxLength="50"
                                            CausesValidation="true" ValidationGroup="VG"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="vldName" runat="server" ControlToValidate="txtName" 
                                            ErrorMessage="The Site Name is required" ValidationGroup="VG">*</asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="vldUniqueName" runat="server" ControlToValidate="txtName" 
                                            ErrorMessage="The Site Name must be unique" ValidationGroup="VG">*</asp:CustomValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Desciption">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtDescription" Text='<%#Bind("Description") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Url">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtUrl" Text='<%#Bind("Url") %>'>
                                            CausesValidation="true" ValidationGroup="VG"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="vldUrl" runat="server" ControlToValidate="txtUrl" 
                                            ErrorMessage="The Site Url is required" ValidationGroup="VG">*</asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnLogins" CommandName="gotoLogins" CommandArgument='<%#Eval("SiteID") %>'
                                            AlternateText="Edit logins for site" ImageUrl="~/WebUI/img/login.jpg" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="edit" ID="btnEdit" ImageUrl="~/WebUI/img/edit.jpg" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="update" ID="btnUpdate" ImageUrl="~/WebUI/img/save.jpg" 
                                            CausesValidation="true" ValidationGroup="VG" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="delete" ID="btnDelete" ImageUrl="~/WebUI/img/delete.jpg"
                                            OnClientClick="return ConfirmDelete('site')" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="cancel" ID="btnCancel" ImageUrl="~/WebUI/img/cancel.jpg" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="gwAltItem" />
                            <HeaderStyle CssClass="gwHeader" />
                            <RowStyle CssClass="gwItem" />
                            <SelectedRowStyle CssClass="gwSelItem" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:ImageButton runat="server" ImageUrl="~/WebUI/img/Add.gif" ID="btnAdd" OnClick="btnAdd_Click" />
                    </td>
                </tr>
            </table>
            <asp:ValidationSummary ID="vsSites" runat="server" ValidationGroup="VG" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
