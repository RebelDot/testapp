﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AppPortal.WebUI.LoginList"
    EnableEventValidation="false" MasterPageFile="~/WebUI/Main.Master" %>

<asp:Content ContentPlaceHolderID="cphTitle" runat="server">
    Login List</asp:Content>
<asp:Content ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblBreadCrumb" runat="server" Text="Avaliable Site" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphBody">
    <asp:Label ID="lblGroups" Text="" runat="server"></asp:Label>
    <asp:DataList runat="server" ID="dlSites" OnItemDataBound="dlSites_DataBound" CssClass="sitesList">
        <ItemTemplate>
            <a href='<%#Eval("Url") %>' class="site">
                <asp:Label runat="server" Text='<%#Eval("Name") %>'></asp:Label></a>
            <asp:DataList runat="server" ID="dlLogins" DataKeyField="LoginID" OnItemDataBound="dlLogins_DataBound"
                OnItemCommand="dlLogin_ItemCommand">
                <ItemTemplate>
                    <asp:Button runat="server" CssClass="login" ID="btnLogin" CommandName="newcomm" CommandArgument='<%#Eval("LoginID") %>'></asp:Button>
                </ItemTemplate>
            </asp:DataList>
            <asp:DataList runat="server" ID="dlLoginToken" DataKeyField="LoginTokenID" OnItemCommand="dlLoginToken_ItemCommand"
                OnItemDataBound="dlLoginToken_DataBound">
                <ItemTemplate>
                    <asp:Button runat="server" ID="btnLoginToken" CssClass="login" CommandName="newcomm" CommandArgument='<%#Eval("LoginTokenID") %>'>
                    </asp:Button>                                        
                </ItemTemplate>
            </asp:DataList>
        </ItemTemplate>
    </asp:DataList>
</asp:Content>
