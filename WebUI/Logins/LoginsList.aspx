﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginsList.aspx.cs" Inherits="AppPortal.WebUI.Logins.LoginsList"
    MasterPageFile="~/WebUI/Main.Master" %>

<asp:Content ContentPlaceHolderID="cphTitle" runat="server">
    List of logins
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblBreadCrumb" Text="Edit Logins" runat="server"></asp:Label>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" runat="server">
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" CssClass="panel">
                <asp:Label ID="lblSelectSite" Text="Select Site:" runat="server"></asp:Label>
                <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSites_SelectedIndexChanged">
                </asp:DropDownList>
            </asp:Panel>
            <br />
            <table width="100%">
                <tr>
                    <td>
                        <asp:Panel runat="server" CssClass="panelGW">
                            <asp:Label runat="server" Text="Logins"></asp:Label></asp:Panel>
                        <asp:GridView Width="100%" runat="server" ID="gwLogins" AutoGenerateColumns="false"
                            CssClass="gw" DataKeyNames="LoginID" OnRowCancelingEdit="gwLogins_RowCancelingEdit"
                            OnRowDeleting="gwLogins_RowDeleting" OnRowEditing="gwLogins_RowEditing" OnRowUpdating="gwLogins_RowUpdating"
                            OnRowDataBound="gwLogins_RowDataBound" HeaderStyle-Wrap="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Url">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtUrl" Text='<%#Bind("Url") %>' Width="90%" MaxLength="1000"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUrl" runat="server" ErrorMessage="Url required"
                                            Text="*" ValidationGroup="vgLogins" ControlToValidate="txtUrl" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Usename">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUsername" runat="server" Text='<%#Eval("Username") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtUsername" Text='<%#Bind("Username") %>' MaxLength="50"
                                            Width="80%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfUser" runat="server" ErrorMessage="Username required"
                                            Text="*" ValidationGroup="vgLogins" ControlToValidate="txtUsername" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Password">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPassword" runat="server" Text='<%#Eval("Password") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtPassword" Text='<%#Bind("Password") %>' MaxLength="50"
                                            Width="80%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfPass" runat="server" ErrorMessage="Password required"
                                            Text="*" ValidationGroup="vgLogins" ControlToValidate="txtPassword" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Security Level" ItemStyle-Width="20">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSecurityLevel" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlSecurityLevel" DataTextField="Name" DataValueField="SecurityLevelID">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="edit" ID="btnEdit" ImageUrl="~/WebUI/img/edit.jpg" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="update" ID="btnUpdate" ImageUrl="~/WebUI/img/save.jpg"
                                            ValidationGroup="vgLogins" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="delete" ID="btnDelete" ImageUrl="~/WebUI/img/delete.jpg"
                                            OnClientClick="return ConfirmDelete('login')" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="cancel" ID="btnCancel" ImageUrl="~/WebUI/img/cancel.jpg" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="gwAltItem" />
                            <HeaderStyle CssClass="gwHeader" />
                            <RowStyle CssClass="gwItem" />
                            <SelectedRowStyle CssClass="gwSelItem" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:ImageButton runat="server" ImageUrl="~/WebUI/img/Add.gif" ID="btnAdd" OnClick="btnAdd_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:ValidationSummary ID="vsLogins" runat="server" ValidationGroup="vgLogins" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:Panel ID="Panel1" runat="server" CssClass="panelGW">
                            <asp:Label ID="Label1" runat="server" Text="Login Tokens"></asp:Label></asp:Panel>
                        <asp:GridView Width="100%" runat="server" ID="gwLoginToken" AutoGenerateColumns="false"
                            CssClass="gw" DataKeyNames="LoginTokenID" OnRowCancelingEdit="gwLoginToken_RowCancelingEdit"
                            OnRowDeleting="gwLoginToken_RowDeleting" OnRowEditing="gwLoginToken_RowEditing"
                            OnRowUpdating="gwLoginToken_RowUpdating" OnRowDataBound="gwLoginToken_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Login Url">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtUrl" Text='<%#Bind("Url") %>' Width="90%" MaxLength="1000"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUrlT" runat="server" ControlToValidate="txtUrl"
                                            ValidationGroup="vgLoginToken" Display="Dynamic" Text="*" ErrorMessage="Url required"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Service Url">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceUrl" runat="server" Text='<%#Eval("ServiceUrl") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtServiceUrl" Text='<%#Bind("ServiceUrl") %>' MaxLength="1000"
                                            Width="90%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSUrlT" runat="server" ControlToValidate="txtServiceUrl"
                                            ValidationGroup="vgLoginToken" Display="Dynamic" Text="*" ErrorMessage="Url for the service is required"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Username">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUsername" runat="server" Text='<%#Eval("Username") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtUsername" Text='<%#Bind("Username") %>'
                                            Width="80%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUsername"
                                            ValidationGroup="vgLoginToken" Display="Dynamic" Text="*" ErrorMessage="Username required"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Static Text"> 
                                    <ItemTemplate>
                                        <asp:Label ID="lblStaticText" runat="server" Text='<%#Eval("StaticText") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" ID="txtStaticText" Text='<%#Bind("StaticText") %>' MaxLength="50"
                                            Width="80%" ToolTip="Enter the static text needed by the service to authenticate the user"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvStaticText" runat="server" ControlToValidate="txtStaticText"
                                            ValidationGroup="vgLoginToken" Display="Dynamic" Text="*" ErrorMessage="The static text is required"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Security Level" ItemStyle-Width="40">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSecurityLevel" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlSecurityLevel" DataTextField="Name" DataValueField="SecurityLevelID">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="edit" ID="btnEdit" ImageUrl="~/WebUI/img/edit.jpg" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="update" ID="btnUpdate" ImageUrl="~/WebUI/img/save.jpg"
                                            ValidationGroup="vgLoginToken" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="10">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="delete" ID="btnDelete" ImageUrl="~/WebUI/img/delete.jpg"
                                            OnClientClick="ConfirmDelete('login')" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CommandName="cancel" ID="btnCancel" ImageUrl="~/WebUI/img/cancel.jpg" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="gwAltItem" />
                            <HeaderStyle CssClass="gwHeader" />
                            <RowStyle CssClass="gwItem" />
                            <SelectedRowStyle CssClass="gwSelItem" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:ImageButton runat="server" ImageUrl="~/WebUI/img/Add.gif" ID="btnAddLoginToken"
                            OnClick="btnAddToken_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:ValidationSummary runat="server" ValidationGroup="vgLoginToken" ID="vsLoginToken" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
