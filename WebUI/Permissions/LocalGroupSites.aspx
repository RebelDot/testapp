﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalGroupSites.aspx.cs" Inherits="AppPortal.WebUI.Permissions.LocalGroupSites" 
    MasterPageFile="~/WebUI/Main.Master"%>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphTitle">
    Local Group Sites</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="server">

    <script type="text/javascript" language="javascript">
        function confApprove()
        {
            resp = window.confirm ('Are you sure you want to deny group access to this site?');
            return resp;
        }

    </script>

</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphBreadCrumb">
     <asp:Label ID="lblGroupName" runat="server" Text="Label" CssClass="pageTitle"></asp:Label>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphBody">
<asp:UpdatePanel ID="updPanel" runat="server">
    <ContentTemplate>
    <div style="text-align:center">
        <asp:HiddenField ID="hdnLocalGroupID" runat="server" />
        <table>
            <tr>
                <td align="left">
                    Select Local Group: 
                    <asp:DropDownList runat="server" ID="ddlLocalGroups" 
                        AutoPostBack="True" DataTextField="GroupName" DataValueField="LocalGroupID"
                        onselectedindexchanged="ddlLocalGroups_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr><td> &nbsp</td></tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlLocalGroupSites" runat="server" CssClass="panelGW">
                        <asp:Label ID="lblTitlePanel" runat="server" Text="Sites Permissions"></asp:Label>
                    </asp:Panel>
                    <asp:GridView ID="grdSites" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="SiteID" onrowcancelingedit="grdSites_RowCancelingEdit" 
                        onrowdeleting="grdSites_RowDeleting" onrowediting="grdSites_RowEditing" 
                        onrowupdating="grdSites_RowUpdating" 
                        onrowdatabound="grdSites_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Site Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblSiteName" runat="server" Text='<%# Eval("SiteName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlSiteName" runat="server" AutoPostBack="true"
                                        DataTextField="Name" DataValueField="SiteID" 
                                        onselectedindexchanged="ddlSiteName_SelectedIndexChanged" ></asp:DropDownList>
                                    <asp:CustomValidator ID="vldSiteName" runat="server" ControlToValidate="ddlSiteName" 
                                        ErrorMessage="Selected Site already configured for this local group" 
                                        ValidationGroup="VG">*</asp:CustomValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Security Level">
                                <ItemTemplate>
                                    <asp:Label ID="lblSecurityLevel" runat="server" Text='<%# Eval("SecurityLevel") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlSecurityLevel" runat="server"
                                        DataTextField="Name" DataValueField="SecurityLevelID" >
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" 
                                        CommandName="Edit" ImageUrl="~/WebUI/img/edit.jpg" ToolTip="Edit" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="btnSave" runat="server" CausesValidation="True" ValidationGroup="VG" 
                                        CommandName="Update" ImageUrl="~/WebUI/img/save.jpg" ToolTip="Save" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" 
                                        CommandName="Delete" ImageUrl="~/WebUI/img/delete.jpg" ToolTip="Delete"
                                        OnClientClick="return confApprove()" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" ImageUrl="~/WebUI/img/cancels.jpg" ToolTip="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="gwAltItem" />
                        <HeaderStyle CssClass="gwHeader" />
                        <RowStyle CssClass="gwItem" />
                        <SelectedRowStyle CssClass="gwSelItem" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <div style="float:left">
                    <asp:ImageButton ID="btnBack" runat="server" ToolTip="Back"
                        onclick="btnBack_Click" ImageUrl="~/WebUI/img/back.jpg" />
                    </div>
                    <asp:ImageButton ID="btnAddSite" runat="server" ImageUrl="~/WebUI/img/Add.gif" 
                        ToolTip="Add Site" onclick="btnAddSite_Click"  />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="vldLocalGroupSites" runat="server" ValidationGroup="VG" />
    </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
