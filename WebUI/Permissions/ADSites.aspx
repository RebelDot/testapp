﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ADSites.aspx.cs" Inherits="AppPortal.WebUI.Permissions.SitesPermissions"
    MasterPageFile="~/WebUI/Main.Master" %>

<asp:Content ContentPlaceHolderID="cphTitle" runat="server">
    Edit active directory accounts sites permissions
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="server">

    <script type="text/javascript" language="javascript">
        function confApprove()
        {
            resp = window.confirm ('Are you sure you want to deny group access to this site?');
            return resp;
        }

    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblPageTitle" Text="Sites Permissions for active directory accounts" CssClass="pageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnAccountID" runat="server" />
            <table>
                <tr>
                    <td colspan="3" align="left">
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label Text="Select account:" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAccountType" runat="server" OnSelectedIndexChanged="ddlAccountType_SelectedIndexChanged"
                            AutoPostBack="True">
                            <asp:ListItem Text="Groups"></asp:ListItem>
                            <asp:ListItem Text="Users"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlAccounts" OnSelectedIndexChanged="ddlAccounts_SelectedIndexChanged"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="left" >
                        <asp:CheckBox ID="chbAccountsShown" runat="server" Text="Show only configured accounts" 
                            AutoPostBack="true" Checked="true" 
                            oncheckedchanged="chbAccountsShown_CheckedChanged" />
                    </td>
                </tr>
                <tr><td>&nbsp</td></tr>
                <tr>
                    <td colspan="3" align="left">
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlADSites" runat="server" CssClass="panelGW">
                                        <asp:Label ID="lblTitlePanel" runat="server" Text="Sites Permissions"></asp:Label>
                                    </asp:Panel>
                                    <asp:GridView runat="server" ID="gwSites" AutoGenerateColumns="False" OnRowCancelingEdit="gwSites_RowCancelingEdit"
                                        OnRowDataBound="gwSites_RowDataBound" OnRowDeleting="gwSites_RowDeleting" OnRowEditing="gwSites_RowEditing"
                                        OnRowUpdating="gwSites_RowUpdating" DataKeyNames="SiteID">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Site Name">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlSiteName" runat="server" DataTextField="Name" DataValueField="SiteID"
                                                        OnSelectedIndexChanged="ddlSiteName_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:CustomValidator ID="vldSiteName" runat="server" ControlToValidate="ddlSiteName" 
                                                        ErrorMessage="Selected Site already configured for this account" 
                                                        ValidationGroup="VG">*</asp:CustomValidator>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSiteName" runat="server" Text='<%# Eval("SiteName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Security Level">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlSecurityLevel" runat="server" DataTextField="Name" DataValueField="SecurityLevelID">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSecurityLevel" runat="server" Text='<%# Eval("SecurityLevel") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                        ImageUrl="~/WebUI/img/edit.jpg" ToolTip="Edit" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="btnSave" runat="server" CausesValidation="True" CommandName="Update"
                                                        ImageUrl="~/WebUI/img/save.jpg" ToolTip="Save" ValidationGroup="VG" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                        ImageUrl="~/WebUI/img/delete.jpg" ToolTip="Delete" OnClientClick="return confApprove()" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                        ImageUrl="~/WebUI/img/cancel.jpg" ToolTip="Cancel" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="gwAltItem" />
                                        <HeaderStyle CssClass="gwHeader" />
                                        <RowStyle CssClass="gwItem" />
                                        <SelectedRowStyle CssClass="gwSelItem" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:ImageButton runat="server" ImageUrl="~/WebUI/img/Add.gif" ID="btnAdd" OnClick="btnAdd_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:ValidationSummary ID="vsLocalGroupSites" runat="server" ValidationGroup="VG" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
