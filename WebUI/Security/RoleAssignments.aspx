﻿<%@ Page Language="C#" MasterPageFile="~/WebUI/Main.Master" AutoEventWireup="true" CodeBehind="RoleAssignments.aspx.cs" Inherits="AppPortal.WebUI.Security.RoleAssignments" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    Roles Assignment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" language="javascript">
        function confApprove()
        {
            resp = window.confirm ('Are you sure you want to remove this assignment?');
            return resp;
        }

    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblRoleAssignment" runat="server" Text="Role Assignment" CssClass="pageTitle"></asp:Label>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
<asp:UpdatePanel ID="updPanel" runat="server">
    <ContentTemplate>
    <div style="text-align:center">
        <table>
            <tr>
                <td>
                    <asp:Panel ID="pnlRoleAssignment" runat="server" CssClass="panelGW">
                        <asp:Label ID="lblTitlePanel" runat="server" Text="Roles assigned:"></asp:Label>
                    </asp:Panel>
                    <asp:GridView ID="grdAccountRoles" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="AccountID" 
                        onrowcancelingedit="grdRoleAssignments_RowCancelingEdit" 
                        onrowediting="grdRoleAssignments_RowEditing" 
                        onrowupdating="grdRoleAssignments_RowUpdating" 
                        onrowdatabound="grdRoleAssignments_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlAccountName" runat="server" AutoPostBack="true"
                                        DataTextField="Name" DataValueField="AccountID" ></asp:DropDownList>
                                    <asp:CustomValidator ID="vldAccountName" runat="server" ControlToValidate="ddlAccountName" 
                                        ErrorMessage="Selected account already has a role assigned" 
                                        ValidationGroup="VG">*</asp:CustomValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlRole" runat="server"
                                        DataTextField="Name" DataValueField="RoleID" >
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" 
                                        CommandName="Edit" ImageUrl="~/WebUI/img/edit.jpg" ToolTip="Edit" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="btnSave" runat="server" CausesValidation="True" ValidationGroup="VG" 
                                        CommandName="Update" ImageUrl="~/WebUI/img/save.jpg" ToolTip="Save" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" ImageUrl="~/WebUI/img/cancel.jpg" ToolTip="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="gwAltItem" />
                        <HeaderStyle CssClass="gwHeader" />
                        <RowStyle CssClass="gwItem" />
                        <SelectedRowStyle CssClass="gwSelItem" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnAddAssignment" runat="server" ImageUrl="~/WebUI/img/Add.gif" 
                        ToolTip="Add Assignment" onclick="btnAddAssignment_Click"  />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vldLocalGroupSites" runat="server" ValidationGroup="VG" />
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
