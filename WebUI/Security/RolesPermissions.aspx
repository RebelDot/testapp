﻿<%@ Page Language="C#" MasterPageFile="~/WebUI/Main.Master" AutoEventWireup="true" CodeBehind="RolesPermissions.aspx.cs" Inherits="AppPortal.WebUI.Security.RolesPermissions" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    Edit Roles Security Permissions
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphBreadCrumb" runat="server">
    <asp:Label ID="lblBreadCrumb" runat="server" Text="Roles Security Permissions" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
<%--    <asp:UpdatePanel ID="Panel1" runat="server">
        <ContentTemplate>--%>
        <table>
            <tr>
                <td>
                    <asp:Panel ID="pnlRolesPermission" runat="server" CssClass="panelGW">
                        <asp:Label ID="lblTitlePanel" runat="server" Text="Roles Permissions"></asp:Label>
                    </asp:Panel>
                    <asp:GridView ID="grdRoles" runat="server" AutoGenerateColumns="False">
                        <Columns>
                            
                        </Columns>
                        <AlternatingRowStyle CssClass="gwAltItem" />
                        <HeaderStyle CssClass="gwHeader" />
                        <RowStyle CssClass="gwItem" />
                        <SelectedRowStyle CssClass="gwSelItem" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
